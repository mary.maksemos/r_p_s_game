# Use an official Python runtime as a parent image
FROM python:3.10-alpine

# Set the working directory to /app
WORKDIR /app

# Copy the entire project directory into the container
COPY . .

# Define the command to run the main.py file when the container starts
CMD ["python3", "rps.py"]
