# Rock, Paper, Scissors

A Python implementation of the popular game Rock, Paper, Scissors.

# Description

This project is a Python implementation of the classic game Rock, Paper, Scissors. The game is played against the computer, where the player chooses to show rock, paper, or scissors. Depending on the choices made by the players, the outcome of the round is determined, and a winner is declared.
The project is developed using Python and can be executed by running the file "rps.py" in a Python environment, such as VS Code.

# Features

User-friendly interface allowing the player to choose their option by entering the corresponding number.
Proper handling of different combinations of the player's and computer's choices to determine the winner.
Presentation of the result for each round and an overall winner.

# Installation

To run the game, make sure you have Python installed on your computer. If you don't have Python installed, you can download it from the Python website and follow the installation instructions for your platform.

# Usage

Clone or download this project to your computer.

Open VS Code or any other Python-compatible editor.

Navigate to the root folder of the project.

Open the "rps.py" file.

Run the file by selecting "Run" or by running the command "python rps.py" in the terminal.

# How to Play

In this game, you'll be playing against the computer.
    The rules are simple:
    - Scissors cuts Paper
    - Paper covers Rock
    - Rock crushes Lizard
    - Lizard poisons Spock
    - Spock smashes Scissors
    - Scissors decapitates Lizard
    - Lizard eats Paper
    - Paper disproves Spock
    - Spock vaporizes Rock
    - Rock crushes Scissors

    >>>You will be playing against the computer.
          Select a difficulty level to begin.
             - Easy: The computer chooses from Rock, Paper, and Scissors only.
             - Hard: The computer chooses from all available options.
  
  

# Additional Features

The Rock, Paper, Scissors Game includes the following features:

Scoring System: The game keeps track of the overall score throughout multiple rounds.

Additional Choices: In addition to rock, paper, and scissors, the game includes extra choices to add diversity and excitement.

Countdown Timer: Each round has a countdown timer, adding a sense of urgency and challenge.

Difficulty Levels: You can choose between "easy" and "hard" modes to adjust the level of challenge.

Improved Code Structure: The code is well-structured and organized for better understanding and maintainability.

Storing Results in a File: You have the option to store the game results in a file for future reference.

Printing Results on Screen: The game displays the results of each round on the screen for immediate feedback.

Tutorial or Instructions: The game provides a tutorial or instructions section to guide new players.


