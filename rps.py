"""
Rock-Paper-Scissors-Lizard-Spock Game

This module implements the Rock-Paper-Scissors-Lizard-Spock game,
allowing the user to play against the computer.
"""

import os
import random
import datetime
import threading

CHOICES = ["rock", "paper", "scissors", "lizard", "spock"]
DIFFICULTY_LEVELS = ["easy", "hard"]
RESULTS_FILE = "game_results.txt"
ROUND_TIME_LIMIT = 10  # Time limit for each round in seconds


def determine_winner(user_choice, computer_choice):
    """
    Determines the winner based on the user's choice and the computer's choice.

    Args:
        user_choice (str): The user's choice.
        computer_choice (str): The computer's choice.

    Returns:
        str: The winner ("user", "computer", or "tie").
    """
    rules = {
        "rock": ["scissors", "lizard"],
        "paper": ["rock", "spock"],
        "scissors": ["paper", "lizard"],
        "lizard": ["paper", "spock"],
        "spock": ["scissors", "rock"]
    }

    if user_choice == computer_choice:
        return "tie"

    if computer_choice in rules[user_choice]:
        return "user"

    return "computer"


def print_choices():
    """
    Prints the available choices for the game.
    """
    print("Available choices:")
    for index, choice in enumerate(CHOICES):
        print(f"{index+1}. {choice.capitalize()}")
    print("0. Quit")


def print_result(winner):
    """
    Prints the result of the round based on the winner.

    Args:
        winner (str): The winner of the round ("user", "computer", or "tie").
    """
    if winner == "user":
        print("You won!")
    elif winner == "computer":
        print("You lost!")
    else:
        print("It's a tie!")


def get_computer_choice(difficulty):
    """
    Generates the computer's choice based on the selected difficulty.

    Args:
        difficulty (str): The selected difficulty level.

    Returns:
        str: The computer's choice.
    """
    if difficulty == "easy":
        return random.choice(CHOICES[:3])

    return random.choice(CHOICES)


def get_user_choice(timer_event):
    """
    Gets the user's choice within the time limit.

    Args:
        timer_event (threading.Event): Event to track the timer.

    Returns:
        str: The user's choice or "timeout" if the timer runs out.
    """
    user_choice = None

    def input_thread():
        nonlocal user_choice
        user_input = input("Enter the number corresponding to your choice: ")
        if user_input.isdigit():
            choice_index = int(user_input) - 1
            if choice_index == -1:
                user_choice = "quit"
            elif 0 <= choice_index < len(CHOICES):
                user_choice = CHOICES[choice_index]
        timer_event.set()  # Signal the timer event to stop waiting

    timer_event.clear()  # Clear the timer event flag

    print_choices()

    input_thread = threading.Thread(target=input_thread)
    input_thread.start()

    timer_event.wait(ROUND_TIME_LIMIT)  # Wait for the timer event or timeout

    if user_choice is not None:
        return user_choice

    print("Time's up!")
    return "timeout"


def clear_screen():
    """
    Clears the console screen.
    """
    if os.name == "nt":  # For Windows
        os.system("cls")
    else:  # For Linux/Unix
        os.system("clear")


def write_result_to_file(result):
    """
    Writes the game result to a file.

    Args:
        result (str): The game result to be written.
    """
    timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    with open(RESULTS_FILE, "a", encoding="utf-8") as file:
        file.write(f"{timestamp} - {result}\n")


def play_round(user_choice, difficulty):
    """
    Plays a single round of the game.

    Args:
        user_choice (str): The user's choice.
        difficulty (str): The selected difficulty level.

    Returns:
        str: The winner of the round ("user", "computer", or "tie").
    """
    computer_choice = get_computer_choice(difficulty)
    print(f"\nComputer chose: {computer_choice.capitalize()}")

    winner = determine_winner(user_choice, computer_choice)
    print_result(winner)

    result = f"The winner is: {winner}!"
    write_result_to_file(result)

    return winner


def display_interface():
    """Display interface."""
    print("   _______   _______  _______  _______ ")
    print("  /       \\ /       \\/       \\/       \\")
    print(" /    R     /     P    \\   S    \\   !  \\")
    print(" \\         \\          /         /      /")
    print("  \\ _______/\\_______/\\_______/\\_______/")
    print("                                        ")
    print()
    print("==============================================")
    print("             Rock Paper Scissors              ")
    print("==============================================")
    print()


def display_tutorial():
    """Display the tutorials."""
    print("===============================================")
    print("             Rock Paper Scissors               ")
    print("              Tutorial  Mode                   ")
    print("===============================================")
    print("Welcome to the Rock Paper Scissors tutorial!")
    print("In this game, you'll be playing against the computer.")
    print("The rules are simple:")
    print(" - Scissors cuts Paper")
    print(" - Paper covers Rock")
    print(" - Rock crushes Lizard")
    print(" - Lizard poisons Spock")
    print(" - Spock smashes Scissors")
    print(" - Scissors decapitates Lizard")
    print(" - Lizard eats Paper")
    print(" - Paper disproves Spock")
    print(" - Spock vaporizes Rock")
    print(" - Rock crushes Scissors")
    print()
    print("You will be playing against the computer."
          "Select a difficulty level to begin.")
    print(" - Easy: The computer chooses from Rock, Paper, and Scissors only.")
    print(" - Hard: The computer chooses from all available options.")
    print()
    print("*               Are you ready to start?             *")
    print("*                    Let's play!                    *")
    print("*                                                   *")
    print("*****************************************************")


def play_game():
    """
    Plays the Rock-Paper-Scissors-Lizard-Spock game.
    """
    clear_screen()
    display_interface()
    display_tutorial()
    difficulty = get_difficulty_level()

    user_score = 0
    computer_score = 0
    rounds = 0

    while True:
        timer_event = threading.Event()  # Event to track the timer
        user_choice = get_user_choice(timer_event)

        if user_choice == "quit":
            break

        if user_choice == "timeout":
            print("You lost the round!")
            winner = "computer"
        else:
            winner = play_round(user_choice, difficulty)

        if winner == "user":
            user_score += 1
        elif winner == "computer":
            computer_score += 1

        rounds += 1

        print()

    print("        Thanks for playing!         ")

    if user_score > computer_score:
        print("************* You win the game! ***************")
    elif user_score < computer_score:
        print("         Computer wins the game!!!!!!!!!!!!!!!!!")
    else:
        print("It's a tie!")

    print(f"\nFinal score - You: {user_score}, Computer: {computer_score}")
    print(">>>>>>>>>>>Total rounds played:", rounds)
    print()
    print("****************************************************")
    print()


def get_difficulty_level():
    """
    Prompts the user to select the difficulty level.

    Returns:
        str: The selected difficulty level.
    """
    while True:
        print("\nSelect the difficulty level:")
        print("1. Easy")
        print("2. Hard")
        user_input = input("Enter the number corresponding to your choice: ")
        if user_input.isdigit():
            choice_index = int(user_input) - 1
        if 0 <= choice_index < len(DIFFICULTY_LEVELS):
            return DIFFICULTY_LEVELS[choice_index]

        print("Invalid input. Please try again.")


if __name__ == "__main__":
    play_game()
