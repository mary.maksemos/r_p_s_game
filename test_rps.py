"""
This module contains unit tests for the rps module.
"""
import unittest
from unittest.mock import patch
import threading
import rps


class GameTestCase(unittest.TestCase):
    """
    Test case for the rps module.
    """
    def test_determine_winner(self):
        """Test the determine_winner function."""
        self.assertEqual(rps.determine_winner("rock", "scissors"), "user")
        self.assertEqual(rps.determine_winner("paper", "rock"), "user")
        self.assertEqual(rps.determine_winner("scissors", "rock"), "computer")
        self.assertEqual(rps.determine_winner("rock", "paper"), "computer")
        self.assertEqual(rps.determine_winner("rock", "rock"), "tie")
        self.assertEqual(rps.determine_winner("paper", "paper"), "tie")

    def test_get_computer_choice(self):
        """Test the get_computer_choice function."""
        with patch("random.choice") as mock_choice:
            mock_choice.return_value = "rock"
            self.assertEqual(rps.get_computer_choice("easy"), "rock")
            mock_choice.return_value = "spock"
            self.assertEqual(rps.get_computer_choice("hard"), "spock")

    def test_get_user_choice(self):
        """Test the get_user_choice function."""
        with patch("builtins.input", return_value="1"):
            timer_event = threading.Event()
            self.assertEqual(rps.get_user_choice(timer_event), "rock")

        with patch("builtins.input", return_value="0"):
            timer_event = threading.Event()
            self.assertEqual(rps.get_user_choice(timer_event), "quit")

        with patch("builtins.input", side_effect=[None]):
            timer_event = threading.Event()
            self.assertEqual(rps.get_user_choice(timer_event), "timeout")

    def test_play_round(self):
        """Test the play_round function."""
        with patch("rps.get_computer_choice", return_value="scissors"):
            winner = rps.play_round("rock", "easy")
            self.assertEqual(winner, "user")

        with patch("rps.get_computer_choice", return_value="rock"):
            winner = rps.play_round("scissors", "easy")
            self.assertEqual(winner, "computer")

        with patch("rps.get_computer_choice", return_value="rock"):
            winner = rps.play_round("rock", "easy")
            self.assertEqual(winner, "tie")


if __name__ == "__main__":
    unittest.main()
